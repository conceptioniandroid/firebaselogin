package com.conceptioni.firebaselogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;

public class Register extends AppCompatActivity {

    private ArrayList<UserModel> user = new ArrayList();
    private ArrayList<String> email = new ArrayList();
    FirebaseFirestore myDB;
    EditText etname, etemail, etphoneno;
    Button btnregister;
    boolean istrue = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        myDB = FirebaseFirestore.getInstance();
        GetFcmData();
        init();
        allclick();
    }

    private void allclick() {
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etname.getText().toString().equalsIgnoreCase("")) {
                    if (!etemail.getText().toString().equalsIgnoreCase("")) {
                        if (!etphoneno.getText().toString().equalsIgnoreCase("")) {
                            if (!user.isEmpty()) {
                                String email1 = etemail.getText().toString();
                                if (!email.contains(email1)){
                                    AddData(etname.getText().toString(), etemail.getText().toString(), etphoneno.getText().toString());
                                }else {
                                    Toast.makeText(Register.this, "User Already Registered Plz try with different mail id", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                AddData(etname.getText().toString(), etemail.getText().toString(), etphoneno.getText().toString());
                            }
                        } else {
                            etphoneno.setError("Please Enter Phone No");
                        }
                    } else {
                        etemail.setError("Please Enter Email");
                    }
                } else {
                    etname.setError("Please Enter Name");
                }
            }
        });
    }

    private void init() {
        btnregister = findViewById(R.id.btnregister);
        etname = findViewById(R.id.etname);
        etemail = findViewById(R.id.etemail);
        etphoneno = findViewById(R.id.etphoneno);
    }

    private void GetFcmData() {

        myDB.collection("User").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (e != null) {
                }
                user.clear();
                email.clear();
                if (queryDocumentSnapshots != null) {
                    for (DocumentSnapshot doc : queryDocumentSnapshots) {
                        //                    list.add(doc.getString("task_name"));
                        UserModel userModel = new UserModel();
                        userModel.setEmail(doc.getString("Email"));
                        userModel.setName(doc.getString("Name"));
                        userModel.setPhoneNo(doc.getString("PhoneNo"));
                        email.add(doc.getString("Email"));
                        user.add(userModel);
                    }
                }
            }
        });
    }

    private void AddData(String name, String email, String phone) {

        final HashMap<String, String> data = new HashMap<>();
        data.put("Email", email);
        data.put("PhoneNo", phone);
        data.put("Name", name);

        myDB.collection("User").add(data).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
//                GetFcmData();
                startActivity(new Intent(Register.this,MainActivity.class));
                Toast.makeText(Register.this, "User Registered Successfully...", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    private boolean checkemailexist() {
        for (int i = 0; i < user.size(); i++) {
            istrue = !etemail.getText().toString().equalsIgnoreCase(user.get(i).getEmail());
        }
        return istrue;
    }
}
